package com.test.alert;

import com.test.http.config.AppConfig;

import java.io.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.IntStream;

class AlertTestUtil {

    static void generateMockLog(int entryAmount, String webSite) {
        String fileLocation = Objects.requireNonNull(AlertTestUtil.class.getClassLoader().getResource(AppConfig.getInputFileLocation())).getFile();
        File sampleFile = new File(fileLocation);

        try (FileWriter fw = new FileWriter(sampleFile, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            IntStream.range(0, entryAmount)
                    .mapToObj(i -> String.format("::1 - - [%s] \"GET %s HTTP/1.1\" 404 204", LocalDateTime.now().toString(), webSite))
                    .forEach(out::println);
        } catch (IOException e) {
            e.printStackTrace();
            //exception handling left as an exercise for the reader
        }
    }
}
