package com.test.http.cache;

import com.test.http.api.parsing.LogEntry;
import com.test.http.config.AppConfig;
import com.test.http.stats.LogAlertHandler;
import com.test.http.stats.LogStatistics;
import org.apache.commons.lang3.StringUtils;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

public class LogEntriesCache extends ConcurrentLinkedQueue<LogEntry> {

    private AtomicLong entryCounter = new AtomicLong(0L);
    private final LogAlertHandler logAlertHandler;

    private Map<String, AtomicLong> MOST_HITS = new ConcurrentHashMap<>();
    private Map.Entry<String, AtomicLong> last_highest_hit = new AbstractMap.SimpleEntry<>("initial", new AtomicLong(0L));


    public LogEntriesCache() {
        super();
        LogStatistics.init(this);
        this.logAlertHandler = new LogAlertHandler();

        Timer timer = new Timer("CacheReset");

        timer.scheduleAtFixedRate(new SharedCacheRest(), 2000L, AppConfig.getAlertInteval());
    }

    @Override
    public boolean add(LogEntry logEntry) {
        boolean added = super.add(logEntry);
        entryCounter.incrementAndGet();

        String webSiteName = logEntry.getSection();

        MOST_HITS.putIfAbsent(webSiteName, new AtomicLong(1L));

        long entryOccurrence = MOST_HITS.get(webSiteName).getAndIncrement();

        if (entryOccurrence > last_highest_hit.getValue().longValue()) {
            last_highest_hit = new AbstractMap.SimpleEntry<>(webSiteName, new AtomicLong(entryOccurrence));
        }

        if (added) {
            checkForAlert(entryCounter.longValue());
        }

        return added;
    }


    public Map.Entry<String, AtomicLong> getHighestHit() {
        synchronized (this) {
            return last_highest_hit;
        }
    }


    public void reset() {
        synchronized (this) {
            entryCounter = new AtomicLong(0L);
        }
    }

    private void checkForAlert(Long updateIntervalValue) {
        String alertMessage = logAlertHandler.put(updateIntervalValue);

        if (StringUtils.isNotEmpty(alertMessage)) {
            System.out.println(alertMessage);
        }
    }

    private class SharedCacheRest extends TimerTask {
        @Override
        public void run() {
            MOST_HITS.clear();
        }
    }

}
