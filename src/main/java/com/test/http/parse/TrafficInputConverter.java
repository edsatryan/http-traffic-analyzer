package com.test.http.parse;

import com.test.http.api.parsing.LogEntry;
import com.test.http.api.parsing.LogLineAware;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TrafficInputConverter implements LogLineAware {
    private static final Pattern pattern = Pattern.compile("^(\\S+) (\\S+) (\\S+) " +
            "\\[([\\w:/]+\\s[+\\-]\\d{4})] \"(\\S+)" +
            " (\\S+)\\s*(\\S+)?\\s*\" (\\d{3}) (\\S+)");


    @Override
    public LogEntry parse(String logLine) {
        if (StringUtils.isEmpty(logLine)) {
            return DUMMY_LINE;
        }

        final Matcher matcher = pattern.matcher(logLine);
        if (matcher.matches()) {
            return new LogEntry.Builder()
                    .setRemoteAddress(matcher.group(1))
                    .setUserIdentity(matcher.group(2))
                    .setFrank(matcher.group(3))
                    .setDateTimeString(matcher.group(4))
                    .setMethodType(matcher.group(5))
                    .setRequestLine(matcher.group(6))
                    .setResponseCode(Integer.valueOf(matcher.group(8)))
                    .setResponseSize(Long.valueOf(matcher.group(9)))
                    .build();
        }

        return DUMMY_LINE;
    }
}
