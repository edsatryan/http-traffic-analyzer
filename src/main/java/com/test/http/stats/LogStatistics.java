package com.test.http.stats;

import com.test.http.cache.LogEntriesCache;
import com.test.http.config.AppConfig;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;

public class LogStatistics {
    private final LogEntriesCache sharedCache;

    private LogStatistics(LogEntriesCache sharedCache) {
        this.sharedCache = sharedCache;
        Timer timer = new Timer("Timer");

        timer.scheduleAtFixedRate(new MaxHitTask(), 2000L, AppConfig.getMaxHitPrintInterval());
    }


    private class MaxHitTask extends TimerTask {
        @Override
        public void run() {
            Map.Entry<String, AtomicLong> maxHit = sharedCache.getHighestHit();
            System.out.println(String.format("Most hit section [%s], occurrence [%d]", maxHit.getKey(), maxHit.getValue().get()));
            sharedCache.reset();
        }
    }


    public static LogStatistics init(LogEntriesCache sharedCache) {
        return new LogStatistics(sharedCache);
    }
}
