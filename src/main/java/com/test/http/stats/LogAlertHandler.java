package com.test.http.stats;

import com.test.http.config.AppConfig;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Queue;

public class LogAlertHandler {

    private Queue<Long> intervalQueue = new LinkedList<>();
    private static final int queueSize = AppConfig.getAlertInteval() / AppConfig.getUpdateInterval().intValue();
    private boolean alertStatus = false;

    public String put(Long updateIntervalValue) {
        intervalQueue.add(updateIntervalValue);
        if (intervalQueue.size() > queueSize) {
            intervalQueue.poll();
        }

        Long total = intervalQueue.stream().mapToLong(i -> i).sum();

        return manageAlert(total);
    }

    private String manageAlert(Long total) {
        boolean alertFlag = total > AppConfig.getEntriesLimit();

        if (alertFlag && !alertStatus) {
            alertStatus = true;
            return String.format("High traffic generated an alert - hits = %s, triggered at %s",
                    total, getCurrentTime());
        } else if (!alertFlag && alertStatus) {
            alertStatus = false;
            return String.format("Traffic back to normal at %s", getCurrentTime());
        }
        return "";
    }

    private String getCurrentTime() {
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now());
    }
}
