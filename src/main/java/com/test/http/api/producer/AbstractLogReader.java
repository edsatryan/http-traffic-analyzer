package com.test.http.api.producer;

import com.test.http.api.LogReader;
import com.test.http.config.AppConfig;

import java.io.File;
import java.io.RandomAccessFile;

public abstract class AbstractLogReader implements LogReader {
    private final File targetLogFile;
    private long logPointer;

    public AbstractLogReader(File file) throws IllegalFileException {
        if (file == null || !file.exists()) {
            throw new IllegalFileException("unable to resolve specified file: " + file);
        }

        if (!file.canRead()) {
            throw new IllegalFileException("can not read from specified log file, please check permissions");
        }

        this.targetLogFile = file;
    }


    @Override
    public void run() {
        try {
            while (AppConfig.RUN_SERVER) {
                Thread.sleep(AppConfig.getUpdateInterval());
                long logFileLength = targetLogFile.length();

                if (logFileLength < logPointer) {
                    this.produce("Log file was reset. Restarting logging from start of file.");
                    logPointer = logFileLength;
                } else if (logFileLength > logPointer) {
                    RandomAccessFile raf = new RandomAccessFile(targetLogFile, "r");
                    raf.seek(logPointer);
                    String line;

                    while ((line = raf.readLine()) != null) {
                        this.produce(line);
                    }

                    logPointer = raf.getFilePointer();
                    raf.close();
                }
            }
        } catch (Exception e) {
            this.produce("Fatal error reading log file, log tailing has stopped.");
        }
    }
}
