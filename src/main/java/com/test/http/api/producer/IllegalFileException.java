package com.test.http.api.producer;

public class IllegalFileException extends RuntimeException {
    private static final long serialVersionUID = 2232071977775187613L;

    public IllegalFileException() {
        super();
    }

    public IllegalFileException(String message) {
        super(message);
    }

    public IllegalFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalFileException(Throwable cause) {
        super(cause);
    }

    protected IllegalFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}