package com.test.http.api.parsing;

public interface LogLineAware {

    LogEntry DUMMY_LINE = new LogEntry.Builder()
            .setRemoteAddress("")
            .setUserIdentity("")
            .setFrank("")
            .setDateTimeString("")
            .setRequestLine("")
            .setResponseCode(400)
            .setResponseSize(0L)
            .build();

    LogEntry parse(String logLine);
}
