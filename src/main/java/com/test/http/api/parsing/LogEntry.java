package com.test.http.api.parsing;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public final class LogEntry {
    private final String remoteAddress;
    private final String userIdentity;
    private final String frank;
    private final String dateTimeString;
    private final String methodType;
    private final String requestLine;
    private final Integer responseCode;
    private final Long responseSize;

    private LogEntry(String remoteAddress, String userIdentity, String frank, String dateTimeString, String methodType, String requestLine, Integer responseCode, Long responseSize) {
        this.remoteAddress = remoteAddress;
        this.userIdentity = userIdentity;
        this.frank = frank;
        this.dateTimeString = dateTimeString;
        this.methodType = methodType;
        this.requestLine = requestLine;
        this.responseCode = responseCode;
        this.responseSize = responseSize;
    }

    public final String getRemoteAddress() {
        if (StringUtils.isNotEmpty(remoteAddress)) {
            return remoteAddress.equals("::1") ? "http://localhost" : remoteAddress;
        }

        return "";
    }

    public final String getUserIdentity() {
        return userIdentity;
    }

    public final String getFrank() {
        return frank;
    }

    public final String getDateTimeString() {
        return dateTimeString;
    }

    public final String getRequestLine() {
        return requestLine;
    }

    public final String getSection() {
        return getRemoteAddress() + "/" + getRequestLine();
    }

    public final String getMethodType() {
        return methodType;
    }

    public final Integer getResponseCode() {
        return responseCode;
    }

    public final Long getResponseSize() {
        return responseSize;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogEntry logEntry = (LogEntry) o;
        return Objects.equals(remoteAddress, logEntry.remoteAddress) &&
                Objects.equals(userIdentity, logEntry.userIdentity) &&
                Objects.equals(frank, logEntry.frank) &&
                Objects.equals(dateTimeString, logEntry.dateTimeString) &&
                Objects.equals(methodType, logEntry.methodType) &&
                Objects.equals(requestLine, logEntry.requestLine) &&
                Objects.equals(responseCode, logEntry.responseCode) &&
                Objects.equals(responseSize, logEntry.responseSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(remoteAddress, userIdentity, frank, dateTimeString, methodType, requestLine, responseCode, responseSize);
    }


    @Override
    public String toString() {
        return "LogEntry{" +
                "remoteAddress='" + remoteAddress + '\'' +
                ", userIdentity='" + userIdentity + '\'' +
                ", frank='" + frank + '\'' +
                ", dateTimeString='" + dateTimeString + '\'' +
                ", methodType='" + methodType + '\'' +
                ", requestLine='" + requestLine + '\'' +
                ", responseCode=" + responseCode +
                ", responseSize=" + responseSize +
                '}';
    }

    public static class Builder {
        private String remoteAddress;
        private String userIdentity;
        private String frank;
        private String dateTimeString;
        private String methodType;
        private String requestLine;
        private Integer responseCode;
        private Long responseSize;


        public Builder setRemoteAddress(String remoteAddress) {
            this.remoteAddress = remoteAddress;
            return this;
        }

        public Builder setUserIdentity(String userIdentity) {
            this.userIdentity = userIdentity;
            return this;
        }

        public Builder setFrank(String frank) {
            this.frank = frank;
            return this;
        }

        public Builder setDateTimeString(String dateTimeString) {
            this.dateTimeString = dateTimeString;
            return this;
        }

        public Builder setMethodType(String methodType) {
            this.methodType = methodType;
            return this;
        }

        public Builder setRequestLine(String requestLine) {
            if (StringUtils.isNotEmpty(requestLine) && requestLine.length() > 1) {
                String[] output = requestLine.split("/");
                this.requestLine = output[1];
            } else {
                this.requestLine = "/";
            }
            return this;
        }

        public Builder setResponseCode(Integer responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        public Builder setResponseSize(Long responseSize) {
            this.responseSize = responseSize;
            return this;
        }

        public LogEntry build() {
            return new LogEntry(
                    remoteAddress,
                    userIdentity,
                    frank,
                    dateTimeString,
                    methodType,
                    requestLine,
                    responseCode,
                    responseSize);
        }
    }
}
