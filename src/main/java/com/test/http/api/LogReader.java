package com.test.http.api;

public interface LogReader extends Runnable {

    void produce(String line);
}
