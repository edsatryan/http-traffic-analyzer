package com.test.http;

import com.test.http.cache.LogEntriesCache;
import com.test.http.config.AppConfig;
import com.test.http.reader.LogFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Program {
    private static final Logger LOGGER = LoggerFactory.getLogger(Program.class);

    public static void main(String[] args) {
        final Thread mainThread = Thread.currentThread();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            AppConfig.RUN_SERVER = false;
            try {
                mainThread.join();
            } catch (InterruptedException ex) {
                LOGGER.error("", ex);
            }
        }));

        LogEntriesCache sharedCache = new LogEntriesCache();
        LogFileReader producer = new LogFileReader(sharedCache);
        new Thread(producer).start();
    }
}
