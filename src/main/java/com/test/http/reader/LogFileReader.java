package com.test.http.reader;

import com.test.http.api.parsing.LogEntry;
import com.test.http.api.parsing.LogLineAware;
import com.test.http.api.producer.AbstractLogReader;
import com.test.http.config.AppConfig;
import com.test.http.parse.TrafficInputConverter;

import java.io.File;
import java.util.AbstractQueue;

import static com.test.http.api.parsing.LogLineAware.DUMMY_LINE;

public class LogFileReader extends AbstractLogReader {

    private LogLineAware logConverter;
    private AbstractQueue<LogEntry> sharedCache;

    public LogFileReader(AbstractQueue<LogEntry> sharedCache) {
        super(new File(AppConfig.getInputFileLocation()));
        this.logConverter = new TrafficInputConverter();
        this.sharedCache = sharedCache;
    }


    @Override
    public void produce(String line) {
        LogEntry logEntry = this.logConverter.parse(line);

        if (!DUMMY_LINE.equals(logEntry)) {
            sharedCache.add(logEntry);
        }
    }
}
