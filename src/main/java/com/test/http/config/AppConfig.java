package com.test.http.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.Map;
import java.util.Properties;

public class AppConfig {

    private static Map<Type, Object> config_cache;
    public static volatile boolean RUN_SERVER = true;

    static {
        Properties prop = new Properties();

        try (InputStream input = ClassLoader.getSystemClassLoader().getResourceAsStream("config.properties")) {
            prop.load(input);

            String inputFileLocation = prop.getProperty("input.location", System.getProperty("user.home") + "/logs/access.log");
            String averageTraffic = prop.getProperty("average.traffic", "1000");
            String threadAmounts = prop.getProperty("processing.threads", "20");
            String updateInterval = prop.getProperty("update.interval", "10000");
            String maxHitPrintInterval = prop.getProperty("maxHit.interval", "10000");
            String alertInterval = prop.getProperty("alert.interval", "20000");
            String entriesLimit = prop.getProperty("entries.limit", "1000");

            config_cache = new EnumMap<>(Type.class);
            config_cache.put(Type.INPUT_LOG_FILE_LOCATION, inputFileLocation);
            config_cache.put(Type.AVERAGE_TRAFFIC_HITS, Integer.valueOf(averageTraffic));
            config_cache.put(Type.PROCESSING_THREADS, Integer.valueOf(threadAmounts));
            config_cache.put(Type.UPDATE_INTERVAL, Long.valueOf(updateInterval));
            config_cache.put(Type.ALERT_INTERVAL, Integer.valueOf(alertInterval));
            config_cache.put(Type.ENTRIES_LIMIT, Long.valueOf(entriesLimit));
            config_cache.put(Type.MAX_HIT_INTERVAL, Long.valueOf(maxHitPrintInterval));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private AppConfig() {
        //static only
    }


    public static String getInputFileLocation() {
        return (String) config_cache.get(Type.INPUT_LOG_FILE_LOCATION);
    }

    public static Integer getAverageTrafficThreshold() {
        return (Integer) config_cache.get(Type.AVERAGE_TRAFFIC_HITS);
    }

    public static Integer getAmountOfProcessingThreads() {
        return (Integer) config_cache.get(Type.PROCESSING_THREADS);
    }

    public static Long getUpdateInterval() {
        return (Long) config_cache.get(Type.UPDATE_INTERVAL);
    }

    public static int getAlertInteval() {
        return (int) config_cache.get(Type.ALERT_INTERVAL);
    }

    public static Long getEntriesLimit() {
        return (Long) config_cache.get(Type.ENTRIES_LIMIT);
    }

    public static Long getMaxHitPrintInterval() {
        return (Long) config_cache.get(Type.MAX_HIT_INTERVAL);
    }

    enum Type {
        INPUT_LOG_FILE_LOCATION,
        AVERAGE_TRAFFIC_HITS,
        PROCESSING_THREADS,
        UPDATE_INTERVAL,
        ALERT_INTERVAL,
        ENTRIES_LIMIT,
        MAX_HIT_INTERVAL
    }
}
